#!/usr/bin/env python3
from beautifultable import BeautifulTable
from dataclasses import dataclass, field
from typing import Dict
import sexpdata

def group_data(data):
    row = []
    for elem in data:
        if len(row) < 2:
            row.append(elem)
        if len(row) == 2:
            yield tuple(row)
            row.clear()

def parse_const(row):
    try:
        return row[1].value(), row[-1]
    except AttributeError:
        print(row)
        raise

def parse_model(data):
    for row in data[1:]:
        yield parse_const(row)

@dataclass
class Task:
    prefix: str
    mode: str = ""
    indices: Dict[int,int] = field(default_factory=dict)
    thread_locals: Dict[str,int] = field(default_factory=dict)
    def matches(self, name):
        return self.prefix in name
    def add_variable(self, name, value):
        prefix, name = name.split(self.prefix, 1)
        if name == "$mode":
            self.mode = "r" if value == 0 else "w"
        elif "$idx$" in name:
            _, name = name.split("$idx$")
            self.indices[int(name)] = value
        else:
            self.thread_locals[prefix] = value

    def get_indices(self):
        return list(self.indices[idx] for idx in range(len(self.indices)))

def structure_state(data):
    task1 = Task(prefix="$T1")
    task2 = Task(prefix="$T2")
    global_state = dict()
    TARGETS = [("$T1", task1), ("$T2", task2), (None, global_state)]
    for k,v in data.items():
        if task1.matches(k):
            task1.add_variable(k, v)
        elif task2.matches(k):
            task2.add_variable(k, v)
        else:
            global_state[k] = v

    return (task1, task2, global_state)

def parse_result(row):
    msg, data = row
    if msg.value() == 'unsat':
        return
    assert msg.value() == 'sat'
    return structure_state(dict(parse_model(data)))


def handle(row, fp_out, expect_race):
    res = parse_result(row)
    found_race = False
    if res is not None:
        print("*** DATA RACE ERROR ***")
        task1, task2, global_state = res

        table = BeautifulTable()
        table.columns.header = ["global"]
        for k, v in sorted(global_state.items()):
            table.rows.append([v], header=k)
        print(table)

        table = BeautifulTable()
        table.columns.header = ["T1", "T2"]
        table.rows.append((task1.mode, task2.mode), header="$access")
        table.rows.append((task1.get_indices(), task2.get_indices()), header="$index")
        for k, v in sorted(task1.thread_locals.items()):
            table.rows.append((v,task2.thread_locals[k]),header=k)

        print(table, file=fp_out)
        print(file=fp_out)
        found_race = True
    return found_race


def show(data, fp_out, expect_race):
    found_race = False
    for row in group_data(data):
        found_race = found_race or handle(row, fp_out, expect_race)
    if not found_race:
        print("Program is data-race free!")
    if found_race ^ expect_race:
        sys.exit(1)

if __name__ == '__main__':
    import sys
    import argparse
    parser = argparse.ArgumentParser(usage="\n* from Z3:\n\tz3 file.smt2 | faial-print\n* from Faial:\n\t./faial -S examples/fail/race1.proto | faial-print")
    parser.add_argument("fp_in",
        metavar="FILENAME",
        nargs='?',
        type=argparse.FileType('r'),
        help="The SMT2 result. Default: standard input (reading from tty yields usage error).",
        default=(None if sys.stdin.isatty() else sys.stdin)
    )
    parser.add_argument("--expect-race",
        action="store_true",
        help="Exit with 0 status when finding a data-race, otherwise exit with error.",
    )
    args = parser.parse_args()
    if args.fp_in is None:
        parser.error("Expecting the result of SMTLIB2.")


    data = "(" + args.fp_in.read() + ")"
    data = sexpdata.loads(data, nil=None, true=None, false=None)
    show(
        data=data,
        fp_out=sys.stdout,
        expect_race=args.expect_race
    )
