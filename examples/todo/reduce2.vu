#|

Reduction #2: Slide 11

Source: https://developer.download.nvidia.com/assets/cuda/files/reduction.pdf

|#

shared g_idata, g_odata, sdata;

const
  local threadIdx.x,
  global blockDim.x,
  local blockIdx.x,
  local tid,
  local i,
  local index;

assert threadIdx.x < blockDim.x;
assert i == blockIdx.x*blockDim.x+ threadIdx.x;
assert distinct [blockIdx.x][threadIdx.x];
prove distinct[i];

assert tid == threadIdx.x;

ro g_idata[i];
rw sdata[blockIdx.x][tid];
sync;

foreach s < blockDim.x {
  assert pow2(s);
  assert index == 2 * s * tid;
  ro sdata[blockIdx.x][index + s] if index < blockDim.x;
  rw sdata[blockIdx.x][index] if index < blockDim.x;
  sync;
}

ro sdata[blockIdx.x][0] if tid == 0;
rw g_odata[blockIdx.x] if tid == 0;
